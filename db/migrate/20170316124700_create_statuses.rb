class CreateStatuses < ActiveRecord::Migration[5.0]
  def change
    create_table :statuses do |t|
      t.boolean :staus
      t.references :post, foreign_key: true

      t.timestamps
    end
  end
end
