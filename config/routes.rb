Rails.application.routes.draw do
   
  
   resources :posts do
    resources :comments 
  end

  resources :users , :attaches, only: [:index, :new, :create, :destroy,:show,:update,:edit] 
  resources :groups ,:usergroups ,only:[:new,:create,:destroy,:index,:update,:show]
  root to:'posts#index'

  get 'groups/new' => "groups#new"
  post 'groups/create'=>"groups#create"
  get 'groups/index'=>"groups/index"

  get 'users/new' => "users#new"
  post 'users/create'=> "users#create"
  get '/login' =>"sessions#new"
  post '/login'=>"sessions#create"
  get '/logout' =>"sessions#destroy"

  get 'usergroups/update'=>"usergroups#update"
  put 'usergroups/:id' =>"usergroups#update"
  
  get 'attaches/new'=>"attaches#new"
  post 'attaches/create'=>"attaches#create"

  get 'users/:id' => "users#show"
  get 'attaches/:id '=> 'attaches#show'


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
