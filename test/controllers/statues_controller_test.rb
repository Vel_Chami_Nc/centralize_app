require 'test_helper'

class StatuesControllerTest < ActionDispatch::IntegrationTest
  test "should get create" do
    get statues_create_url
    assert_response :success
  end

  test "should get update" do
    get statues_update_url
    assert_response :success
  end

end
