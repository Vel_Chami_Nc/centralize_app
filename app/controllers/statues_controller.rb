class StatuesController < ApplicationController
  def create
  	@post=Post.find(params[:post_id])
  	@status=@post.statues.create(status_params)
    redirect_to post_path(@post)
  end

  def update
  	@Post = Post.find(params[:post_id])
    @statues = @Post.statues.find(params[:id])
    @statues.update(status_params)
  end

  def status_params
    params.require(:status).permit(:status)
  end
end
