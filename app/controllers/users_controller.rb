
class UsersController < ApplicationController
  before_action :require_user, only:[:show,:index]
  def index
    @posts=Post.all
    @users=User.all.paginate(page: params[:page],per_page: 5)

  end

  def show
    @user=User.find(params[:id])
    @posts=@user.posts    
    @attaches=@user.attaches
  end

  def create
    @group=Group.new
    @user=User.new(user_params)
    if @user.save
      redirect_to '/'
    else
      render :new
    end

  end

  def destroy
     @user=User.find(params[:id])
     @user.destroy
     redirect_to users_url
  end

  def update
   @user.update(status: params[:status])
    redirect_to :back, notice: 'Successfully updated'
  end

  def new
    @user=User.new
    @group=Group.new
  end
end
private
def user_params
  params.require(:user).permit(:name,:role,:email,:password,:user_id,:group_id,:status)
end