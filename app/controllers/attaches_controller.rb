class AttachesController < ApplicationController
 
  def new
    @attach=current_user.attaches.build
  end

  def create
    @attach=current_user.attaches.build(attaches_params)
    if @attach.save
      redirect_to attach_url(@attach)
    else
      render :new
    end
  end


  def destroy
  end

  def update
    @attach=Attach.find(params[:id])
    @attach.update(attaches_params)
    redirect_to attach_url
  end

  def show
    @attach=Attach.find(params[:id])
  end

  def index
    @attaches=Attach.all
  end

  def edit
    @attach=Attach.find(params[:id])
  end

end

private
def attaches_params
  params.require(:attach).permit(:name,:attachment,:attachment1,:attachment2,:attachment3,:name1,:name2,:name3)
end
