class GroupsController < ApplicationController
  def index
    @groups=Group.all
    @users=User.new
  end

  def show
  end

  def create
    @group=current_user.groups.build(group_params)  
    if @group.save
      redirect_to '/'
    else
      render :new
    end
  end

  def destroy
  end

  def update
   
  end

  def new
    @group=current_user.groups.build
  end
end

private
def group_params
  params.require(:group).permit(:user_id,:group_id)
end
