module StatuesHelper

	def build_read_status_link post
		if post.status == true 
			link_to 'Un-valid', status_path(status, status: false), method: :put
		else
			link_to 'valid', status_path(status, status: true), method: :put
		end
	end

end
