module UsersHelper
	def build_read_status_link user
		if user.status == true 
			link_to 'Un-valid', post_path(@post, status: false), method: :put
		else
			link_to 'valid', post_path(@post, status: true), method: :put
		end
	end
end
