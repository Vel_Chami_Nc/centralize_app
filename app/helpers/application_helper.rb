module ApplicationHelper
	def status_converter(status, truthy: 'Active', falsey: 'Pending')
		if status
			truthy
		else
			falsey
		end
	end

	def full_title(page_title = '')
    base_title = "centralize"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end
end
