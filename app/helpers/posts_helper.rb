module PostsHelper
	def status_weight post
		post.status == false ?  'font-color:red' : 'font-color:green'
	end
	def build_read_status_link post
		if post.status == true 
			link_to 'Un-valid', post_path(post, status: false), method: :put
		else
			link_to 'valid', post_path(post, status: true), method: :put
		end
	end
end
