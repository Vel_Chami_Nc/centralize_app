class Post < ApplicationRecord
	has_many :statues
	has_many :comments
	belongs_to :user, optional: true
	validates :user_id , presence: true , allow_nil: true
end
