class User < ApplicationRecord
	has_many :user_groups
	has_many :groups, through: :user_groups
	has_many :posts    , dependent: :destroy
    has_many :attaches , dependent: :destroy
    validates :name , presence: true
    validates_format_of :email,:with => /\A[^@\s]+@([^@\s]+\.)+[^@\s]+\z/ 
    validates :email , uniqueness: true , on: :create
    validates :name , uniqueness: true , on: :create
    validates :password , :length => { :minimum=>6 } 

	 def staff? 
	  self.role == 'staff' 
	end

	def student? 
	  self.role == 'student' 
	end
	
	def admin? 
	  self.role == 'admin' 
	end

end
