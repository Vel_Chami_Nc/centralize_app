class Attach < ApplicationRecord
	belongs_to :user 
	mount_uploader :attachment, AttachmentUploader
	mount_uploader :attachment1, AttachmentUploader
	mount_uploader :attachment2, AttachmentUploader
	mount_uploader :attachment3, AttachmentUploader # Tells rails to use this uploader for this model.
    validates :attachment, :name, presence: true 
end
